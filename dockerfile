FROM node:15.2.1-alpine3.10

WORKDIR /usr/src/app

LABEL maintainer:bleukoty@gmail.com description:'helps you to parse json to xlsx file'

COPY package*.json ./

RUN npm install && mkdir tmp && npm i -g pm2

COPY ./build .

EXPOSE 3000

ENTRYPOINT [ "node" ]
CMD [ "index.js" ]