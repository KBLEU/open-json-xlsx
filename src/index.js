import express from 'express';
import XLSX from 'xlsx';

var bodyParser = require('body-parser')

const app = express();
const fs = require('fs') ;

app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Welcome on json parser to XLSX file');
});

app.post('/parse-to-xlsx', (req, res) => {

    const workbook = XLSX.utils.book_new();
    const { file_name, tabs } = req.body;

    res.setHeader("content-type", 'application/octet-stream;charset=utf-8');
    res.setHeader("Content-Disposition", 'attachment; filename="' + file_name + '"' + "; filename*=UTF-8''" + file_name + "");

     /* Add worksheets to the workbook */
    createTabs(tabs, workbook);
    const outputfile = `./tmp/${Date.now()}_out.xlsx`;
    
    XLSX.writeFile(workbook, outputfile);
    const readFile = fs.createReadStream(outputfile);
    readFile.pipe(res);
    readFile.on('end', function () {
        res.end();
        console.log('je suis ici');
        fs.unlink(outputfile, () => console.log('file is deleted...'));
    });
});

const createTabs = (tabs, workbook) => {
    tabs.forEach(tab => {
        const {name: ws_name, headers, rows} = tab;
        // worksheet data
        const wsData = [ headers, ...rows];
        // worksheet
        const ws = XLSX.utils.aoa_to_sheet(wsData);
        // add new worksheet to workbook
        XLSX.utils.book_append_sheet(workbook, ws, ws_name);
    });
}

app.listen(3000, () => console.log('app is running on port 3000'));